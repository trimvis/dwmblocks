//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" ", "statusmusic", 5, 18},
	{" ", "updates", 600, 17},
	{" ", "cpu", 5, 16},
	{" ", "memuse", 5, 15},
	{" ", "bluetooth-status", 5, 14},
	{" ", "wlan", 5, 13},
	{" ", "volume", 20, 12},
	{" ", "brightness", 20, 11},
	{" ", "battery", 5, 10},
	{" ", "timedate", 5, 9},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
